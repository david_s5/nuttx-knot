#! /bin/bash

# otherwise default to nuttx
if [ -z "$NUTTX_DOCKER_REPO" ]; then
	NUTTX_DOCKER_REPO="nuttx/nuttx-build-environment:2017-06-10"
fi

# docker hygiene

#Delete all stopped containers (including data-only containers)
#docker rm $(docker ps -a -q)

#Delete all 'untagged/dangling' (<none>) images
#docker rmi $(docker images -q -f dangling=true)

echo "NUTTX_DOCKER_REPO: $NUTTX_DOCKER_REPO";

PWD=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
SRC_DIR=$PWD/../

CCACHE_DIR=${HOME}/.ccache
mkdir -p "${CCACHE_DIR}"

X11_TMP=/tmp/.X11-unix

docker run -it --rm -w "${SRC_DIR}" \
	-e AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}" \
	-e AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}" \
	-e BRANCH_NAME="${BRANCH_NAME}" \
	-e CCACHE_DIR="${CCACHE_DIR}" \
	-e CI="${CI}" \
	-e DISPLAY="${DISPLAY}" \
	-e GIT_SUBMODULES_ARE_EVIL=1 \
	-e LOCAL_USER_ID="$(id -u)" \
	-e TRAVIS_BRANCH="${TRAVIS_BRANCH}" \
	-e TRAVIS_BUILD_ID="${TRAVIS_BUILD_ID}" \
	-v ${CCACHE_DIR}:${CCACHE_DIR}:rw \
	-v ${SRC_DIR}:${SRC_DIR}:rw \
	-v ${X11_TMP}:${X11_TMP}:ro \
	${NUTTX_DOCKER_REPO} /bin/bash -c "$@"
