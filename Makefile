TOP_DIR := ${shell pwd | sed -e 's/ /\\ /g'}
TESTLIST_DIR :=${TOP_DIR}/test-lists
NUTTX_DIR = ${TOP_DIR}/nuttx
NUTTX_TOOLS_DIR = ${NUTTX_DIR}/tools
MISC_TOOLS_DIR = ${TOP_DIR}/misc/tools
TEST_SCRIPT = testbuild.sh
TEST_SCRIPT_ARGS= -l -x


checks_stm32f7:
	cd ${NUTTX_DIR} &&	${NUTTX_TOOLS_DIR}/${TEST_SCRIPT} ${TEST_SCRIPT_ARGS} ${TESTLIST_DIR}/stm32f7 && cd ${TOP_DIR}

checks_stm32:
	cd ${NUTTX_DIR} && ${NUTTX_TOOLS_DIR}/${TEST_SCRIPT} ${TEST_SCRIPT_ARGS} ${TESTLIST_DIR}/stm32 && cd ${TOP_DIR}

checks_kinetis:
	cd ${NUTTX_DIR} && 	${NUTTX_TOOLS_DIR}/${TEST_SCRIPT} ${TEST_SCRIPT_ARGS} ${TESTLIST_DIR}/kinetis && cd ${TOP_DIR}

submodulesclean:
	@git submodule foreach --quiet --recursive git clean -ff -x -d
	@git submodule update --quiet --init --recursive --force || true
	@git submodule sync --recursive
	@git submodule update --init --recursive --force

submodulesupdate:
	@git submodule update --quiet --init --recursive || true
	@git submodule sync --recursive
	@git submodule update --init --recursive

distclean: submodulesclean
	@git clean -ff -x -d -e ".project" -e ".cproject"
